---
layout: post
title:  "Apasih RPL Itu?"
date:   2020-02-17 14:10:10 +0700
categories: post/id
---

Rekayasa Perangkat Lunak di Indonesia dijadikan disiplin ilmu yang dipelajari mulai tingkat Sekolah Menengah Kejuruan sampai tingkatan Perguruan Tinggi. Di tingkat SMK, jurusan ini sudah memiliki kurikulum materi pelajaran sendiri yang sudah ditentukan oleh Dinas Pendidikan. Rekayasa Perangkat Lunak di tingkat SMK biasanya mempelajari materi-materi seperti Bahasa Pemrograman, Desain Web, Pengetahuan tetang Undang Undang HAKI dan ITE, dan sebagainya, tergantung dari sekolah dan kurikulum tiap tahunnya.

source: Wikipedia