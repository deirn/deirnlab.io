---
layout: page
title: Projects
exec: ls
permalink: /projects
---

<ul class="post-list">
  <li>
    <span class="post-meta">Current; Web Application Backend; Go</span>
    <h2><span class="post-link">SINTK</span></h2>
  </li>
  <li>
    <span class="post-meta">Jul 2019 - Oct 2019; Web Application; PHP/CodeIgniter/Laravel</span>
    <h2><a class="post-link" href="http://cekotechnology.com">Cekotechnology</a></h2>
  </li>
</ul>